from mongoengine import *

connect('tumblelog')

class User(Document):
    email = StringField(required=True)
    first_name = StringField(max_length=50)
    last_name = StringField(max_length=50)
    password = StringField(max_length=200)

class Comment(Document):
    content = StringField()
    name = StringField(max_length=120)
	
class Post(Document):
    title = StringField(max_length=120, required=True)
    author = ReferenceField(User)
    tags = StringField(max_length=30)
    comments = ReferenceField(Comment)
    meta = {'allow_inheritance': True}



